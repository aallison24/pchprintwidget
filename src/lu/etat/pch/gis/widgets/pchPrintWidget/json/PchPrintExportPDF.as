package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    /**
     *
     * @author schullto
     */
    public class PchPrintExportPDF {
        private var _colorspace:Number=0;
        private var _compressed:Boolean=true;
        private var _embedFonts:Boolean=true;
        private var _exportMeasureInfo:Boolean=false;
        private var _exportPDFLayersAndFeatureAttributes:Number=0;
        private var _exportPictureSymbolOptions:Number=2;
        private var _imageCompression:Number=0;
        private var _polyginizeMarkers:Boolean=false;

        /**
         * esriExportColorspaceRGB = 0;
         * esriExportColorspaceCMYK = 1
         * @return
         */
        public function get colorspace():Number {
            return _colorspace;
        }

        /**
         * esriExportColorspaceRGB = 0;
         * esriExportColorspaceCMYK = 1
         * @param value
         */
        public function set colorspace(value:Number):void {
            _colorspace=value;
        }

        /**
         *  esriExportImageCompressionNone = 0;
         *  esriExportImageCompressionRLE = 1;
         *  esriExportImageCompressionDeflate = 2;
         *  esriExportImageCompressionLZW = 3;
         *  esriExportImageCompressionJPEG = 4;
         *  esriExportImageCompressionAdaptive = -2147483648;
         * @return
         */
        public function get compressed():Boolean {
            return _compressed;
        }

        /**
         *  esriExportImageCompressionNone = 0;
         *  esriExportImageCompressionRLE = 1;
         *  esriExportImageCompressionDeflate = 2;
         *  esriExportImageCompressionLZW = 3;
         *  esriExportImageCompressionJPEG = 4;
         *  esriExportImageCompressionAdaptive = -2147483648;
         * @param value
         */
        public function set compressed(value:Boolean):void {
            _compressed=value;
        }

        /**
         *
         * @return
         */
        public function get embedFonts():Boolean {
            return _embedFonts;
        }

        /**
         *
         * @param value
         */
        public function set embedFonts(value:Boolean):void {
            _embedFonts=value;
        }

        /**
         *
         * @return
         */
        public function get exportMeasureInfo():Boolean {
            return _exportMeasureInfo;
        }

        /**
         *
         * @param value
         */
        public function set exportMeasureInfo(value:Boolean):void {
            _exportMeasureInfo=value;
        }

        /**
         * esriExportPDFLayerOptionsNone = 0;
         * esriExportPDFLayerOptionsLayersOnly = 1;
         * esriExportPDFLayerOptionsLayersAndFeatureAttributes = 2;
         * @return
         */
        public function get exportPDFLayersAndFeatureAttributes():Number {
            return _exportPDFLayersAndFeatureAttributes;
        }

        /**
         * esriExportPDFLayerOptionsNone = 0;
         * esriExportPDFLayerOptionsLayersOnly = 1;
         * esriExportPDFLayerOptionsLayersAndFeatureAttributes = 2;
         * @param value
         */
        public function set exportPDFLayersAndFeatureAttributes(value:Number):void {
            _exportPDFLayersAndFeatureAttributes=value;
        }

        /**
         * esriPSORasterize = 0;
         * esriPSORasterizeIfRasterData = 1;
         * esriPSOVectorize = 2;
         * @return
         */
        public function get exportPictureSymbolOptions():Number {
            return _exportPictureSymbolOptions;
        }

        /**
         * esriPSORasterize = 0;
         * esriPSORasterizeIfRasterData = 1;
         * esriPSOVectorize = 2;
         * @param value
         */
        public function set exportPictureSymbolOptions(value:Number):void {
            _exportPictureSymbolOptions=value;
        }

        /**
         *
         * @return
         */
        public function get imageCompression():Number {
            return _imageCompression;
        }

        /**
         *
         * @param value
         */
        public function set imageCompression(value:Number):void {
            _imageCompression=value;
        }

        /**
         *
         * @return
         */
        public function get polyginizeMarkers():Boolean {
            return _polyginizeMarkers;
        }

        /**
         *
         * @param value
         */
        public function set polyginizeMarkers(value:Boolean):void {
            _polyginizeMarkers=value;
        }
    }
}