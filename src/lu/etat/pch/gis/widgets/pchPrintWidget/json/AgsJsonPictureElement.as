package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    public class AgsJsonPictureElement extends AgsJsonSymbol {

        public function AgsJsonPictureElement(pictureUrl:String="") {
            type="pchPictureElement";
            this.pictureUrl=pictureUrl;
        }

        private var _pictureUrl:String;

        public function get pictureUrl():String {
            return _pictureUrl;
        }

        public function set pictureUrl(value:String):void {
            _pictureUrl=value;
        }
    }
}