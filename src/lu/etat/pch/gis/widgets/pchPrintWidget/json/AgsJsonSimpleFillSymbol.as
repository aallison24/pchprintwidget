package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    import com.esri.ags.symbols.SimpleFillSymbol;
    /**
     *
     * @author schullto
     */
    public class AgsJsonSimpleFillSymbol extends AgsJsonSymbol {

        /**
         *
         * @param style
         * @param color
         * @param outline
         */
        public function AgsJsonSimpleFillSymbol(style:String="solid", color:AgsJsonRGBColor=null, outline:AgsJsonSimpleLineSymbol=null) {
            type="esriSFS";
            this.style=style
            if(validStyles.indexOf(_style)<0) {
                trace("!!!!!! invalid style used for AgsJsonSimpleFillSymbol: "+style+" !!!!!!");
            }
            this.color=color;
            this.outline=outline;
        }

        private static var validStyles:Array=[ "esriSFSSolid", "esriSFSNull", "esriSFSHollow", "esriSFSHorizontal", "esriSFSVertical", "esriSFSForwardDiagonal", "esriSFSBackwardDiagonal", "esriSFSCross", "esriSFSDiagonalCross" ];

        public static function getObjectFromSimpleFillSymbol(sfs:SimpleFillSymbol):AgsJsonSimpleFillSymbol {
            if(sfs==null)
                return null;
            var color:AgsJsonRGBColor=AgsJsonRGBColor.getObjectFromUINT(sfs.color, sfs.alpha);
            var outline:AgsJsonSimpleLineSymbol=AgsJsonSimpleLineSymbol.getFromObject(sfs.outline);
            var retSym:AgsJsonSimpleFillSymbol=new AgsJsonSimpleFillSymbol(sfs.style, color, outline);
            return retSym;
        }

        private var _color:AgsJsonRGBColor;
        private var _outline:AgsJsonSimpleLineSymbol;
        private var _style:String;

        /**
         *
         * @return
         */
        public function get color():AgsJsonRGBColor {
            return _color;
        }

        /**
         *
         * @param value
         */
        public function set color(value:AgsJsonRGBColor):void {
            _color=value;
        }

        /**
         *
         * @return
         */
        public function get outline():AgsJsonSimpleLineSymbol {
            return _outline;
        }

        /**
         *
         * @param value
         */
        public function set outline(value:AgsJsonSimpleLineSymbol):void {
            _outline=value;
        }

        /**
         * int esriSFSSolid = 0;
         * int esriSFSNull = 1;
         * int esriSFSHollow = 1;
         * int esriSFSHorizontal = 2;
         * int esriSFSVertical = 3;
         * int esriSFSForwardDiagonal = 4;
         * int esriSFSBackwardDiagonal = 5;
         * int esriSFSCross = 6;
         * int esriSFSDiagonalCross = 7;
         * @return
         */
        public function get style():String {
            return _style;
        }

        /**
         * int esriSFSSolid = 0;
         * int esriSFSNull = 1;
         * int esriSFSHollow = 1;
         * int esriSFSHorizontal = 2;
         * int esriSFSVertical = 3;
         * int esriSFSForwardDiagonal = 4;
         * int esriSFSBackwardDiagonal = 5;
         * int esriSFSCross = 6;
         * int esriSFSDiagonalCross = 7;
         * @param value
         */
        public function set style(value:String):void {
            _style=type+value.substring(0, 1).toUpperCase()+value.substring(1).toLowerCase();
            if(validStyles.indexOf(_style)<0) {
                trace("!!!!!! invalid style used for AgsJsonSimpleFillSymbol: "+value+" !!!!!!");
            }
        }
    }
}