package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    public class AgsJsonNumberFormat {
/*
		private Integer roundingOption;
		private Integer roundingValue;
		private Integer alignmentOption;
		private Integer alignmentWidth;
		private Boolean showPlusSign;
		private Boolean useSeparator;
		private Boolean zeroPad;

*/		
        public function AgsJsonNumberFormat(roundingOption:Number=0, roundingValue:Number=3) {
            this._roundingOption=roundingOption;
            this._roundingValue=roundingValue;
            super();
        }

        private var _roundingOption:Number; //esriRoundingOptionEnum
		private var _roundingValue:Number;
		private var _alignmentOption:Number;
		private var _alignmentWidth:Number;
		private var _showPlusSign:Boolean;
		private var _useSeparator:Boolean;
		private var _zeroPad:Boolean;

		public function get zeroPad():Boolean
		{
			return _zeroPad;
		}

		public function set zeroPad(value:Boolean):void
		{
			_zeroPad = value;
		}

		public function get useSeparator():Boolean
		{
			return _useSeparator;
		}

		public function set useSeparator(value:Boolean):void
		{
			_useSeparator = value;
		}

		public function get showPlusSign():Boolean
		{
			return _showPlusSign;
		}

		public function set showPlusSign(value:Boolean):void
		{
			_showPlusSign = value;
		}

		public function get alignmentWidth():Number
		{
			return _alignmentWidth;
		}

		public function set alignmentWidth(value:Number):void
		{
			_alignmentWidth = value;
		}

		public function get alignmentOption():Number
		{
			return _alignmentOption;
		}

		public function set alignmentOption(value:Number):void
		{
			_alignmentOption = value;
		}

        public function get roundingOption():Number {
            return _roundingOption;
        }

        public function set roundingOption(value:Number):void {
            _roundingOption=value;
        }

        public function get roundingValue():Number {
            return _roundingValue;
        }

        public function set roundingValue(value:Number):void {
            _roundingValue=value;
        }
    }
}