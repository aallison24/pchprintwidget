package lu.etat.pch.gis.widgets.pchPrintWidget.json
{
	public class AgsJsonGridLabelFormat
	{
		private var _fontName:String;
		private var _fontSize:Number;
		private var _color:AgsJsonRGBColor;
		private var _labelOrientationTop:Boolean;
		private var _labelOrientationLeft:Boolean;
		private var _labelOrientationRight:Boolean;
		private var _labelOrientationBottom:Boolean;
		private var _format:AgsJsonNumericFormat;
		private var	_labelOffset:Number;

		public function AgsJsonGridLabelFormat()
		{
		}

		public function get fontName():String
		{
			return _fontName;
		}

		public function set fontName(value:String):void
		{
			_fontName = value;
		}

		public function get fontSize():Number
		{
			return _fontSize;
		}

		public function set fontSize(value:Number):void
		{
			_fontSize = value;
		}

		public function get color():AgsJsonRGBColor
		{
			return _color;
		}

		public function set color(value:AgsJsonRGBColor):void
		{
			_color = value;
		}

		public function get labelOrientationTop():Boolean
		{
			return _labelOrientationTop;
		}

		public function set labelOrientationTop(value:Boolean):void
		{
			_labelOrientationTop = value;
		}

		public function get labelOrientationLeft():Boolean
		{
			return _labelOrientationLeft;
		}

		public function set labelOrientationLeft(value:Boolean):void
		{
			_labelOrientationLeft = value;
		}

		public function get labelOrientationRight():Boolean
		{
			return _labelOrientationRight;
		}

		public function set labelOrientationRight(value:Boolean):void
		{
			_labelOrientationRight = value;
		}

		public function get labelOrientationBottom():Boolean
		{
			return _labelOrientationBottom;
		}

		public function set labelOrientationBottom(value:Boolean):void
		{
			_labelOrientationBottom = value;
		}

		public function get format():AgsJsonNumericFormat
		{
			return _format;
		}

		public function set format(value:AgsJsonNumericFormat):void
		{
			_format = value;
		}

		public function get labelOffset():Number
		{
			return _labelOffset;
		}

		public function set labelOffset(value:Number):void
		{
			_labelOffset = value;
		}


	}
}