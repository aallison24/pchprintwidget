package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    public class AgsJsonSymbol {
        private var _type:String="agsJsonSymbol";

        public function get type():String {
            return _type;
        }

        public function set type(value:String):void {
            _type=value;
        }
    }
}
