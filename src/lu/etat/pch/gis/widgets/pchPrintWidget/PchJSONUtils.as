package lu.etat.pch.gis.widgets.pchPrintWidget {
    import com.esri.ags.Graphic;
    import com.esri.ags.Map;
    import com.esri.ags.geometry.Extent;
    import com.esri.ags.geometry.MapPoint;
    import com.esri.ags.geometry.Polyline;
    import com.esri.ags.symbols.*;
    import com.esri.ags.symbols.Symbol;
    
    import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonPictureMarkerSymbol;
    import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonRGBColor;
    import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonSimpleFillSymbol;
    import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonSimpleLineSymbol;
    import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonSimpleMarkerSymbol;
    import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonTextSymbol;
    
    import mx.controls.Alert;

    public class PchJSONUtils {
        public static function bufferPoint(mapPoint:MapPoint, bufferSize:Number):Extent {
            var extent:Extent=new Extent();
            extent.xmin=mapPoint.x-bufferSize;
            extent.ymin=mapPoint.y-bufferSize;
            extent.xmax=mapPoint.x+bufferSize;
            extent.ymax=mapPoint.y+bufferSize;
            return extent;
        }

        public static function decodeMapPoint(mappointObj:Object):MapPoint {
            var mappoint:MapPoint=new MapPoint();
            mappoint.x=mappointObj.x;
            mappoint.y=mappointObj.y;
            return mappoint;
        }

        public static function decodePolyline(polylineObj:Object):Polyline {
            var polyline:Polyline=new Polyline();
            var pathArrayObj:Array=polylineObj.paths;
            for(var i:int=0; i<pathArrayObj.length; i++) {
                var pointArray:Array=new Array();
                var pointArrayObj:Array=pathArrayObj[i];
                for(var j:int=0; j<pointArrayObj.length; j++) {
                    var pointObj:Object=pointArrayObj[j];
                    pointArray.push(new MapPoint(pointObj.x, pointObj.y));
                }
                polyline.addPath(pointArray);
            }
            return polyline;
        }

        public static function encodeGraphic(graphic:Graphic):Object {
            var graObj:Object=new Object();
            graObj.geometry=graphic.geometry;
			if(graphic.symbol)
            	graObj.symbol=encodeSymbol(graphic.symbol);
            return graObj;
        }

        public static function encodeSymbol(symbol:Symbol):Object {
            var symbolObj:Object=new Object();
            if(symbol is TextSymbol) {
				return AgsJsonTextSymbol.getObjectFromTextSymbol(symbol as TextSymbol);
            } else if(symbol is SimpleMarkerSymbol) {
				return AgsJsonSimpleMarkerSymbol.getFromObject(symbol as SimpleMarkerSymbol);
            } else if(symbol is SimpleLineSymbol) {
				return AgsJsonSimpleLineSymbol.getFromObject(symbol as SimpleLineSymbol);
			} else if(symbol is SimpleFillSymbol) {
				return AgsJsonSimpleFillSymbol.getObjectFromSimpleFillSymbol(symbol as SimpleFillSymbol);
			} else if(symbol is PictureMarkerSymbol) {
				return AgsJsonPictureMarkerSymbol.getFromObject(symbol as PictureMarkerSymbol);
			}
			Alert.show("JSONUtils.Unknown SYMBOL type to convert to json: "+symbol);
            return null;
        }
    }
}